import {DirectorsListComponent }  from "./directors-list/directors-list.component.js";

angular
	.module("neotek.directors",[])
	.component(DirectorsListComponent.selector, DirectorsListComponent)
	.config(route);

function route($stateProvider) {

	$stateProvider.state({
		name: 'directors',
		url: '/directors',
		template: `<ui-view></ui-view>`,
		onEnter: function(){

		}
	});

	$stateProvider.state({
		name: 'directors.list',
		url: '/list',
		template: `<directors-list></directors-list>`,
		onEnter: function(){

		}
	});
}