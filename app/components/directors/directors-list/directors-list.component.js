/*import "./styles.scss";*/
import _ from "lodash";
import template from "./directors-list.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "directorsList",
	template
})
class DirectorsListComponent {

	/* @ngInject */
	constructor($state, Directors) {
		this.services = {$state, Directors};
		this.directors = [];
		this.editableListOptions = {
			modelName: "name",
			placeholder: "Имя режиссера",
			getLabel(item){
				return item.name;
			}
		};
	}

	$onInit() {
		this.getDirectorsList();
	}

	getDirectorsList(){
		const {Directors} = this.services;
		this.promise = Directors.api().getList().then(directors=>{
			this.directors = directors;
		});
	}

	addNew(){
		const {Directors} = this.services;
		this.promise = Directors.api().post({}).then(genre=>{
			this.directors.push(genre);
		});
	}

	updateDirector(genre){
		this.promise = genre.put().then();
	}

	removeDirector(data){
		this.promise = data.item.remove().then(()=>{
			this.directors.splice(data.ix, 1);
		});
	}
}

export {DirectorsListComponent};
