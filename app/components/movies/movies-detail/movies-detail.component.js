import "./styles.scss";
import template from "./movies-detail.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "moviesDetail",
	template
})
class MoviesDetailComponent {

	/* @ngInject */
	constructor($state, $stateParams, Movies) {
		this.services = {$state, $stateParams, Movies};
		this.movieData = $stateParams.movieData;
	}

	$onInit(){
		if(!this.movieData){
			this.promise = this.getInitialMovie();
		}
	}

	getInitialMovie(){
		const {Movies, $stateParams} = this.services;

		return Movies.getOne($stateParams.id).then(movie=>{
			this.movieData = movie;
			console.log(this.movieData)
		});
	}

	editMovie(){
		const {$state} = this.services;
		$state.transitionTo("movies.edit", {
			id: this.movieData.id,
			movieData: this.movieData
		});
	}

}

export {MoviesDetailComponent};
