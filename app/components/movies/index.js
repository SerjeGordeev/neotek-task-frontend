import { MoviesService }            from "../backend/services/movies.service.js";
import { MovieTemplate }            from "../backend/factories/movie.factory.js";
import { MoviesListComponent }      from "./movies-list/movies-list.component.js";
import { MoviesListItemComponent }  from "./movie-list-item/movie-list-item.component.js";
import { MoviesDetailComponent }    from "./movies-detail/movies-detail.component.js";
import { MovieEditorComponent }     from "./movie-editor/movie-editor.component.js";

angular
	.module("neotek.movies",[])
	.service("Movies", MoviesService)
	.factory("MovieTemplate", MovieTemplate)
	.component(MoviesListComponent.selector,        MoviesListComponent)
	.component(MoviesListItemComponent.selector,    MoviesListItemComponent)
	.component(MoviesDetailComponent.selector,      MoviesDetailComponent)
	.component(MovieEditorComponent.selector,       MovieEditorComponent)
	.config(route);

function route($stateProvider) {

	$stateProvider.state({
		name: 'movies',
		url: '/movies',
		template: `<ui-view></ui-view>`,
		onEnter: function(){

		}
	});

	$stateProvider.state({
		name: 'movies.list',
		url: '/list',
		template: `<movies-list></movies-list>`,
		onEnter: function(){

		}
	});

	$stateProvider.state({
		name: 'movies.create',
		url: '/create',
		template: `<movie-editor></movie-editor>`,
		onEnter: function(){

		}
	});

	$stateProvider.state({
		name: 'movies.edit',
		url: '/list/:id/edit',
		template: `<movie-editor></movie-editor>`,
		onEnter: function(){

		},
		params: {
			id: null,
			movieData: null
		}
	});

	$stateProvider.state({
		name: 'movies.detail',
		url: '/list/:id',
		template: `<movies-detail></movies-detail>`,
		onEnter: function(){

		},
		params: {
			id: null,
			movieData: null
		}
	});
}