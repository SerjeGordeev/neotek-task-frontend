import "./styles.scss";
import _ from "lodash";
import template from "./movie-editor.template.html";
import {Component} from "main/decorators";

import addIcon from "images/ic_add_black_24px.svg";
import removeIcon from "images/ic_black_24px .svg";

@Component({
	selector: "movieEditor",
	template
})
class MovieEditorComponent {

	/* @ngInject */
	constructor($state, $stateParams, $q, Genres, Actors, Directors, Countries, Movies, MovieTemplate, Attachments, flashAlert) {
		this.services = {$state, $stateParams, $q, Genres, Actors, Directors, Countries, Movies, MovieTemplate, Attachments, flashAlert};
		this.icons = {addIcon, removeIcon};
		this.movieData = $stateParams.movieData || MovieTemplate.getTemplate();
		this.editMode = !!$stateParams.id;

		this.appSelectOptions = {
			multiple: true,
			enableSearch: true,
			enableAll: false,
			getLabel(item){
				return item.name
			}
		}
	}

	$onInit(){
		const {$q, Genres, Actors, Directors, Countries} = this.services;

		this.promise = $q.all({
			genres: Genres.api().getList(),
			actors: Actors.api().getList(),
			filmDirectors: Directors.api().getList(),
			countries: Countries.api().getList(),
			movieData: this.editMode? this.getInitialMovie(): null
		}).then(response=>{
			this.genres = response.genres;
			this.actors = response.actors;
			this.filmDirectors = response.filmDirectors;
			this.countries = response.countries;
		})
	}

	addMovie(){
		const {$state, Movies} = this.services;
		Movies.api().post(this.movieData).then(movie=>{
			$state.transitionTo("movies.list");
		});
	}

	getInitialMovie(){
		const {Movies, $stateParams} = this.services;

		return Movies.getOne($stateParams.id).then(movie=>{
			this.movieData = movie;
		});
	}
	
	onModelChange(model, fieldName){
		this.movieData[fieldName] = _.map(model, "id");
	}

	updateMovie(){
		const {flashAlert} = this.services;
		this.promise = this.movieData.put().then(()=>{
			flashAlert.success("Даные обновлены");
		})
	}

	uploadFile(file){
		const {Attachments} = this.services;
		this.promise = Attachments.upload(file).then((attachment)=>{
			this.movieData.previewImage = attachment.url;
			this.movieData.previewImageId = attachment.id;
		});
	}

	removePreview(){
		this.movieData.previewImage = null;
		this.movieData.previewImageId = null;
	}
}

export {MovieEditorComponent};
