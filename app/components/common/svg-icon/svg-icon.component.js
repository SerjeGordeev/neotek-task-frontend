import "./styles.scss";
import template from "./svg-icon.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "svgIcon",
	bindings: {
		encodedStr: "<"
	},
	template
})
class SvgIconComponent {
	/* @ngInject */
	constructor($sce) {
		this.services = {$sce};
	}

	$onInit() {
		this.data = this.getSvg(this.encodedStr);
	}

	getSvg(encoded){
		const {$sce} = this.services;
		if(/base64/i.test(encoded)){
			return $sce.trustAsHtml(atob(encoded.replace("data:image/svg+xml;base64,","")));
		}
		console.warn("urs-svg: Invalid svg data");
		return "Invalid Data";
	}
}

export {SvgIconComponent};