import "./styles.scss";
import _ from "lodash";
import template from "./app-footer.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "appFooter",
	template
})
class AppFooterComponent {

	/* @ngInject */
	constructor(Movies) {
		this.services = {Movies};
	}

}

export {AppFooterComponent};
