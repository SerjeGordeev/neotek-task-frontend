import {GenresListComponent }  from "./genres-list/genres-list.component.js";

angular
	.module("neotek.genres",[])
	.component(GenresListComponent.selector, GenresListComponent)
	.config(route);

function route($stateProvider) {

	$stateProvider.state({
		name: 'genres',
		url: '/genres',
		template: `<ui-view></ui-view>`,
		onEnter: function(){

		}
	});

	$stateProvider.state({
		name: 'genres.list',
		url: '/list',
		template: `<genres-list></genres-list>`,
		onEnter: function(){

		}
	});
}