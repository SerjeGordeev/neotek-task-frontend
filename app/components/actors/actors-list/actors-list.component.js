import template from "./actors-list.template.html";
import {Component} from "main/decorators";

@Component({
	selector: "actorsList",
	template
})
class ActorsListComponent {

	/* @ngInject */
	constructor($state, Actors) {
		this.services = {$state, Actors};
		this.actors = [];
		this.editableListOptions = {
			modelName: "name",
			placeholder: "Имя актера",
			getLabel(item){
				return item.name;
			}
		};
	}

	$onInit() {
		this.getActorsList();
	}

	getActorsList(){
		const {Actors} = this.services;
		this.promise = Actors.api().getList().then(actors=>{
			this.actors = actors;
		});
	}

	addNew(){
		const {Actors} = this.services;
		this.promise = Actors.api().post({}).then(actor=>{
			this.actors.push(actor);
		});
	}

	updateActor(actor){
		this.promise = actor.put().then();
	}

	removeActor(data){
		this.promise = data.item.remove().then(()=>{
			this.actors.splice(data.ix, 1);
		});
	}
}

export {ActorsListComponent};
