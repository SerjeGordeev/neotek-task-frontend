import { CountriesListComponent }  from "./countries-list/countries-list.component.js";

angular
	.module("neotek.countries",[])
	.component(CountriesListComponent.selector, CountriesListComponent)
	.config(route);

function route($stateProvider) {

	$stateProvider.state({
		name: 'countries',
		url: '/countries',
		template: `<ui-view></ui-view>`,
		onEnter: function(){

		}
	});

	$stateProvider.state({
		name: 'countries.list',
		url: '/list',
		template: `<countries-list></countries-list>`,
		onEnter: function(){

		}
	});
}